﻿Public Class mm_diy   '漠漠DIY功能库
    Dim dm As mycx
    Public Sub New(ByRef mm As mycx) '构造函数, 按引用传递一个默默对象进来
        'dm = New mycx   '创建漠漠对象
        dm = mm
    End Sub

    Protected Overrides Sub Finalize() '析构函数
        'dm = Nothing
    End Sub

    'Public Sub setPath(ByVal baseDmPath As String)
    '    dm.SetPath(baseDmPath)
    'End Sub

    Public Function Random(ByVal Min, ByVal Max)
        Randomize()
        Random = Int((Max - Min + 1) * Rnd() + Min)
    End Function

    Public Sub KeyPressStr(ByVal key_str)
        dm.EnableRealKeypad(1)
        dm.KeyPressStr(key_str, Random(50, 180))
    End Sub

    Public Function WaitBindEx(ByVal str_print, ByVal hwnd, ByVal SetTime) As Integer  '返回结果是0或者1
        Dim i As Integer, result As Integer
        dm.UnBindWindow()
        For i = 1 To SetTime * 10
            result = dm.BindWindowEx(hwnd, "normal", "normal", "normal", "", 101)
            dm.Delay(200)
            If result = 1 Then
                dm.SetWindowState(hwnd, 1)
                Console.WriteLine("绑定【" & str_print & "】成功.并激活.")
                dm.Delay(200)
                'dm.MoveWindow hwnd, x, y//移动窗口到屏幕左上
                Return result
            Else
                Console.WriteLine("绑定【" & str_print & "】失败.")
            End If
            dm.Delay(100)
        Next
        Return result
    End Function

    Public Function WaitFont(x1, y1, x2, y2, str_string, color_format, font_name, font_size, flag, SetWaitTime) As String
        Dim ret As Array, i As Integer, result As String, id, strX, strY
        For i = 1 To SetWaitTime * 10
            result = dm.FindStrWithFontE(x1, y1, x2, y2, str_string, color_format, 1.0, font_name, font_size, flag)
            If result <> "-1|-1|-1" Then
                ret = Split(result, "|")
                If Int(ret(0)) >= 0 Then
                    id = ret(0) : strX = ret(1) : strY = ret(2)
                End If
                Exit For
            End If
            dm.Delay(100)
        Next
        Console.WriteLine("等字【" & str_string & "】定时【" & SetWaitTime & "】秒; 序号【" & id & "】, 坐标: strX=【" & strX & "】 ,strY=【" & strY & "】")
        Return result
    End Function

    Public Function WaitBoot(str_string, SetWaitTime, Mode) As Integer
        Dim ret As Array, i As Integer, result As Integer, str_result As String, id, strX, strY
        For i = 1 To SetWaitTime * 10
            If Mode = 0 Then
                str_result = dm.FindStrWithFontE(817, 184, 1018, 262, str_string, "00fc00-000000|60fc60-000000", 1.0, "宋体", 9, 0)
            ElseIf Mode = 1 Then
                str_result = dm.FindStrWithFontE(263, 353, 765, 538, str_string, "00fc00-000000", 1.0, "宋体", 11, 0)
            End If
            If str_result <> "-1|-1|-1" Then
                ret = Split(str_result, "|")
                If Int(ret(0)) >= 0 Then
                    '                Select Case Mode
                    '                Case 0
                    '                    id = ret(0) : RstrX = ret(1) : RstrY = ret(2)
                    '                    Console.WriteLing "等字【" & string & "】定时【" & SetWaitTime & "】秒; 序号【" & id & "】, 坐标: RstrX=【" & RstrX & "】 ,RstrY=【" & RstrY & "】"
                    '                Case 1
                    '                    id = ret(0) : DstrX = ret(1) : DstrY = ret(2)
                    '                    Console.WriteLing "等字【" & string & "】定时【" & SetWaitTime & "】秒; 序号【" & id & "】, 坐标: DstrX=【" & DstrX & "】 ,DstrY=【" & DstrY & "】"
                    '                End Select
                    id = ret(0) : strX = ret(1) + 1 : strY = ret(2) + 1
                    Console.WriteLine("等字【" & str_string & "】定时【" & SetWaitTime & "】秒; 序号【" & id & "】, 坐标: strX=【" & strX & "】 ,strY=【" & strY & "】")
                End If
                result = 1
                Exit For
            Else
                result = 0
            End If
            dm.Delay(100)
        Next
        Return result
    End Function

    Public Function WaitImg(print_str, SetImgs, SetWaitTime, setFindWidth, setFindHeight, ByRef getImgX, ByRef getImgY) As Integer
        Dim i As Integer, result As Integer
        Dim x, y
        'Console.WriteLine(dm.GetPath())
        For i = 1 To SetWaitTime * 10
            result = dm.FindPic(0, 0, setFindWidth, setFindHeight, SetImgs, "101010", 0.9, 0, x, y)
            getImgX = x : getImgY = y
            If result > -1 Then
                Exit For
            End If
            dm.Delay(100)
        Next
        Console.WriteLine("等图【" & print_str & "】定时【" & SetWaitTime & "】秒寻找【" & SetImgs & "】; 序号【" & result & "】, 坐标 : ImgX=【" & getImgX & "】 ,ImgY=【" & getImgY & "】")
        Return result
    End Function

    Public Sub LClick(x, y, w, h, mode)
        Select Case mode
            Case 0
                dm.EnableRealMouse(2, 12, 30)
                dm.MoveToEx(x, y, w, h)
                dm.Delay(Random(200, 400))
                dm.LeftDown()
                dm.Delay(Random(30, 80))
                dm.LeftUp()
                dm.Delay(Random(30, 80))
            Case 1
                dm.EnableRealMouse(2, 12, 30)
                dm.MoveToEx(x, y, w, h)
                dm.Delay(Random(200, 400))
                dm.LeftDown()
                dm.Delay(Random(30, 80))
                dm.LeftUp()
                dm.Delay(Random(30, 80))
                dm.MoveToEx(x - 140, y - 140, 80, 80)
        End Select
    End Sub

    Public Sub RClick(x, y, w, h)
        dm.EnableRealMouse(2, 12, 30)
        dm.MoveToEx(x, y, w, h)
        dm.Delay(Random(200, 400))
        dm.RightDown()
        dm.Delay(Random(30, 80))
        dm.RightUp()
    End Sub

    Public Sub KPress(key)
        dm.EnableRealKeypad(1)
        dm.KeyPressChar(key)
    End Sub

    Public Sub Alt键(key)
        'E=69(背包)   Z=90(自动战斗)  G=71(收服宝宝)
        dm.KeyDown(18)
        dm.Delay(Random(300, 500))
        dm.KeyPress(key)
        dm.Delay(Random(100, 150))
        dm.KeyUp(18)
        dm.Delay(Random(1000, 1500))
    End Sub


End Class
