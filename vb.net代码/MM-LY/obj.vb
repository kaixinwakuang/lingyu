Public Class mycx
    Private obj As Object

    Public Sub New() '构造函数
        obj = CreateObject("fy.mycx")
    End Sub

    Protected Overrides Sub Finalize() '析构函数
        obj = Nothing
    End Sub

    Public Function Delay(ByVal mis As Integer) As Integer
        Delay = obj.mTMAW(mis)
    End Function

    Public Function SetMouseDelay(ByVal tpe As String,ByVal delay As Integer) As Integer
        SetMouseDelay = obj.jdKrTS(tpe,delay)
    End Function

    Public Function GetKeyState(ByVal vk As Integer) As Integer
        GetKeyState = obj.lZPi(vk)
    End Function

    Public Function FindPicS(ByVal x1 As Integer,ByVal y1 As Integer,ByVal x2 As Integer,ByVal y2 As Integer,ByVal pic_name As String,ByVal delta_color As String,ByVal sim As Double,ByVal dir As Integer,<Runtime.InteropServices.Out()> ByRef x As Object,<Runtime.InteropServices.Out()> ByRef y As Object) As String
        FindPicS = obj.TEoAT(x1,y1,x2,y2,pic_name,delta_color,sim,dir,x,y)
    End Function

    Public Function EnableMouseSync(ByVal en As Integer,ByVal time_out As Integer) As Integer
        EnableMouseSync = obj.tsbYeWfhhLld(en,time_out)
    End Function

    Public Function SetMinColGap(ByVal col_gap As Integer) As Integer
        SetMinColGap = obj.cNNTSqPZmMF(col_gap)
    End Function

    Public Function ReadIntAddr(ByVal hwnd As Integer,ByVal addr As Integer,ByVal tpe As Integer) As Integer
        ReadIntAddr = obj.XxlgBpQPgodM(hwnd,addr,tpe)
    End Function

    Public Function EnableKeypadSync(ByVal en As Integer,ByVal time_out As Integer) As Integer
        EnableKeypadSync = obj.MQhvVJrYqHAwUfQ(en,time_out)
    End Function

    Public Function GetFileLength(ByVal file_name As String) As Integer
        GetFileLength = obj.cqQAuKgS(file_name)
    End Function

    Public Function GetPicSize(ByVal pic_name As String) As String
        GetPicSize = obj.pYFmfqcUq(pic_name)
    End Function

    Public Function WriteDouble(ByVal hwnd As Integer,ByVal addr As String,ByVal v As Double) As Integer
        WriteDouble = obj.HinlerL(hwnd,addr,v)
    End Function

    Public Function Capture(ByVal x1 As Integer,ByVal y1 As Integer,ByVal x2 As Integer,ByVal y2 As Integer,ByVal file_name As String) As Integer
        Capture = obj.xXjioJZh(x1,y1,x2,y2,file_name)
    End Function

    Public Function SetPicPwd(ByVal pwd As String) As Integer
        SetPicPwd = obj.vJUbMMYuWyrJiS(pwd)
    End Function

    Public Function FetchWord(ByVal x1 As Integer,ByVal y1 As Integer,ByVal x2 As Integer,ByVal y2 As Integer,ByVal color As String,ByVal word As String) As String
        FetchWord = obj.KNeVR(x1,y1,x2,y2,color,word)
    End Function

    Public Function GetForegroundWindow() As Integer
        GetForegroundWindow = obj.FHBnqQKs()
    End Function

    Public Function SendString2(ByVal hwnd As Integer,ByVal str As String) As Integer
        SendString2 = obj.ndncmdPKD(hwnd,str)
    End Function

    Public Function LeftDown() As Integer
        LeftDown = obj.ZdNBGsx()
    End Function

    Public Function GetWordResultCount(ByVal str As String) As Integer
        GetWordResultCount = obj.kspSiU(str)
    End Function

    Public Function FaqFetch() As String
        FaqFetch = obj.qfMRkeyic()
    End Function

    Public Function FoobarTextRect(ByVal hwnd As Integer,ByVal x As Integer,ByVal y As Integer,ByVal w As Integer,ByVal h As Integer) As Integer
        FoobarTextRect = obj.CFyLtkoX(hwnd,x,y,w,h)
    End Function

    Public Function FindDouble(ByVal hwnd As Integer,ByVal addr_range As String,ByVal double_value_min As Double,ByVal double_value_max As Double) As String
        FindDouble = obj.jAeK(hwnd,addr_range,double_value_min,double_value_max)
    End Function

    Public Function WheelDown() As Integer
        WheelDown = obj.EWdycYhfBIW()
    End Function

    Public Function FoobarClose(ByVal hwnd As Integer) As Integer
        FoobarClose = obj.DJfnCVe(hwnd)
    End Function

    Public Function GetWindowClass(ByVal hwnd As Integer) As String
        GetWindowClass = obj.yYBpzNCVUKFYSR(hwnd)
    End Function

    Public Function WheelUp() As Integer
        WheelUp = obj.xauDlvSysL()
    End Function

    Public Function GetClientSize(ByVal hwnd As Integer,<Runtime.InteropServices.Out()> ByRef width As Object,<Runtime.InteropServices.Out()> ByRef height As Object) As Integer
        GetClientSize = obj.CkRXl(hwnd,width,height)
    End Function

    Public Function ReadDouble(ByVal hwnd As Integer,ByVal addr As String) As Double
        ReadDouble = obj.IUcecYoXvK(hwnd,addr)
    End Function

    Public Function FindPicMem(ByVal x1 As Integer,ByVal y1 As Integer,ByVal x2 As Integer,ByVal y2 As Integer,ByVal pic_info As String,ByVal delta_color As String,ByVal sim As Double,ByVal dir As Integer,<Runtime.InteropServices.Out()> ByRef x As Object,<Runtime.InteropServices.Out()> ByRef y As Object) As Integer
        FindPicMem = obj.JLXIf(x1,y1,x2,y2,pic_info,delta_color,sim,dir,x,y)
    End Function

    Public Function EnableKeypadMsg(ByVal en As Integer) As Integer
        EnableKeypadMsg = obj.bYZTQCP(en)
    End Function

    Public Function GetScreenDataBmp(ByVal x1 As Integer,ByVal y1 As Integer,ByVal x2 As Integer,ByVal y2 As Integer,<Runtime.InteropServices.Out()> ByRef data As Object,<Runtime.InteropServices.Out()> ByRef size As Object) As Integer
        GetScreenDataBmp = obj.sDRmAtqCQUMRjs(x1,y1,x2,y2,data,size)
    End Function

    Public Function EnableIme(ByVal en As Integer) As Integer
        EnableIme = obj.IMvvYhtFR(en)
    End Function

    Public Function SetDisplayAcceler(ByVal level As Integer) As Integer
        SetDisplayAcceler = obj.jHCM(level)
    End Function

    Public Function FoobarSetFont(ByVal hwnd As Integer,ByVal font_name As String,ByVal size As Integer,ByVal flag As Integer) As Integer
        FoobarSetFont = obj.gixC(hwnd,font_name,size,flag)
    End Function

    Public Function WriteIni(ByVal section As String,ByVal key As String,ByVal v As String,ByVal file_name As String) As Integer
        WriteIni = obj.dlTsMaLnwJdbjc(section,key,v,file_name)
    End Function

    Public Function LoadPic(ByVal pic_name As String) As Integer
        LoadPic = obj.SSfyvcTLrXSyMNX(pic_name)
    End Function

    Public Function WriteFloat(ByVal hwnd As Integer,ByVal addr As String,ByVal v As Single) As Integer
        WriteFloat = obj.gNlTQUejK(hwnd,addr,v)
    End Function

    Public Function CreateFoobarCustom(ByVal hwnd As Integer,ByVal x As Integer,ByVal y As Integer,ByVal pic As String,ByVal trans_color As String,ByVal sim As Double) As Integer
        CreateFoobarCustom = obj.RsVl(hwnd,x,y,pic,trans_color,sim)
    End Function

    Public Function SetMemoryFindResultToFile(ByVal file_name As String) As Integer
        SetMemoryFindResultToFile = obj.oIsy(file_name)
    End Function

    Public Function FoobarStartGif(ByVal hwnd As Integer,ByVal x As Integer,ByVal y As Integer,ByVal pic_name As String,ByVal repeat_limit As Integer,ByVal delay As Integer) As Integer
        FoobarStartGif = obj.JUbDXZm(hwnd,x,y,pic_name,repeat_limit,delay)
    End Function

    Public Function FindStrE(ByVal x1 As Integer,ByVal y1 As Integer,ByVal x2 As Integer,ByVal y2 As Integer,ByVal str As String,ByVal color As String,ByVal sim As Double) As String
        FindStrE = obj.AhjDtdJRJzzxzm(x1,y1,x2,y2,str,color,sim)
    End Function

    Public Function WriteData(ByVal hwnd As Integer,ByVal addr As String,ByVal data As String) As Integer
        WriteData = obj.MxLIclPUEeYsd(hwnd,addr,data)
    End Function

    Public Function EnumIniKeyPwd(ByVal section As String,ByVal file_name As String,ByVal pwd As String) As String
        EnumIniKeyPwd = obj.HkxBRUMheGwxUu(section,file_name,pwd)
    End Function

    Public Function FaqCaptureFromFile(ByVal x1 As Integer,ByVal y1 As Integer,ByVal x2 As Integer,ByVal y2 As Integer,ByVal file_name As String,ByVal quality As Integer) As Integer
        FaqCaptureFromFile = obj.yBpdeDKEHoZ(x1,y1,x2,y2,file_name,quality)
    End Function

    Public Function FindStr(ByVal x1 As Integer,ByVal y1 As Integer,ByVal x2 As Integer,ByVal y2 As Integer,ByVal str As String,ByVal color As String,ByVal sim As Double,<Runtime.InteropServices.Out()> ByRef x As Object,<Runtime.InteropServices.Out()> ByRef y As Object) As Integer
        FindStr = obj.wfLnylegDBZNAZ(x1,y1,x2,y2,str,color,sim,x,y)
    End Function

    Public Function GetNowDict() As Integer
        GetNowDict = obj.NRpYmUWL()
    End Function

    Public Function DisableScreenSave() As Integer
        DisableScreenSave = obj.idHJ()
    End Function

    Public Function CaptureGif(ByVal x1 As Integer,ByVal y1 As Integer,ByVal x2 As Integer,ByVal y2 As Integer,ByVal file_name As String,ByVal delay As Integer,ByVal time As Integer) As Integer
        CaptureGif = obj.iQTe(x1,y1,x2,y2,file_name,delay,time)
    End Function

    Public Function Play(ByVal file_name As String) As Integer
        Play = obj.oWIVzhJvyfH(file_name)
    End Function

    Public Function Reg(ByVal code As String,ByVal ver As String) As Integer
        Reg = obj.BnQo(code,ver)
    End Function

    Public Function ReadStringAddr(ByVal hwnd As Integer,ByVal addr As Integer,ByVal tpe As Integer,ByVal length As Integer) As String
        ReadStringAddr = obj.jPcWQphlYZlLzk(hwnd,addr,tpe,length)
    End Function

    Public Function UseDict(ByVal index As Integer) As Integer
        UseDict = obj.LuwRfjfXrCukWlE(index)
    End Function

    Public Function FindStrWithFontE(ByVal x1 As Integer,ByVal y1 As Integer,ByVal x2 As Integer,ByVal y2 As Integer,ByVal str As String,ByVal color As String,ByVal sim As Double,ByVal font_name As String,ByVal font_size As Integer,ByVal flag As Integer) As String
        FindStrWithFontE = obj.AxES(x1,y1,x2,y2,str,color,sim,font_name,font_size,flag)
    End Function

    Public Function GetWordResultStr(ByVal str As String,ByVal index As Integer) As String
        GetWordResultStr = obj.yoWCBvWKubBR(str,index)
    End Function

    Public Function CaptureJpg(ByVal x1 As Integer,ByVal y1 As Integer,ByVal x2 As Integer,ByVal y2 As Integer,ByVal file_name As String,ByVal quality As Integer) As Integer
        CaptureJpg = obj.TAUtXVMd(x1,y1,x2,y2,file_name,quality)
    End Function

    Public Function GetMachineCodeNoMac() As String
        GetMachineCodeNoMac = obj.nAMLJwZctyuv()
    End Function

    Public Function GetTime() As Integer
        GetTime = obj.gEYFrXSjsbWzmM()
    End Function

    Public Function BGR2RGB(ByVal bgr_color As String) As String
        BGR2RGB = obj.oCmbpofTzTfxWr(bgr_color)
    End Function

    Public Function EnumIniKey(ByVal section As String,ByVal file_name As String) As String
        EnumIniKey = obj.VQljqJfToBktM(section,file_name)
    End Function

    Public Function EnableMouseMsg(ByVal en As Integer) As Integer
        EnableMouseMsg = obj.iFwQP(en)
    End Function

    Public Function GetPath() As String
        GetPath = obj.PlVabpydnLAHRy()
    End Function

    Public Function DeleteFile(ByVal file_name As String) As Integer
        DeleteFile = obj.lDyRp(file_name)
    End Function

    Public Function SetEnumWindowDelay(ByVal delay As Integer) As Integer
        SetEnumWindowDelay = obj.dAvSG(delay)
    End Function

    Public Function CreateFoobarRoundRect(ByVal hwnd As Integer,ByVal x As Integer,ByVal y As Integer,ByVal w As Integer,ByVal h As Integer,ByVal rw As Integer,ByVal rh As Integer) As Integer
        CreateFoobarRoundRect = obj.cQfSglca(hwnd,x,y,w,h,rw,rh)
    End Function

    Public Function LockDisplay(ByVal locks As Integer) As Integer
        LockDisplay = obj.JpwSYuUGat(locks)
    End Function

    Public Function InitCri() As Integer
        InitCri = obj.xjPTljElgcDhrF()
    End Function

    Public Function KeyDownChar(ByVal key_str As String) As Integer
        KeyDownChar = obj.zfWYkDzFQbTaZ(key_str)
    End Function

    Public Function GetWindow(ByVal hwnd As Integer,ByVal flag As Integer) As Integer
        GetWindow = obj.kLiiMG(hwnd,flag)
    End Function

    Public Function FindIntEx(ByVal hwnd As Integer,ByVal addr_range As String,ByVal int_value_min As Integer,ByVal int_value_max As Integer,ByVal tpe As Integer,ByVal steps As Integer,ByVal multi_thread As Integer,ByVal mode As Integer) As String
        FindIntEx = obj.ubLeVUVC(hwnd,addr_range,int_value_min,int_value_max,tpe,steps,multi_thread,mode)
    End Function

    Public Function ExitOs(ByVal tpe As Integer) As Integer
        ExitOs = obj.mHcmPNEYCUKUfqw(tpe)
    End Function

    Public Function ForceUnBindWindow(ByVal hwnd As Integer) As Integer
        ForceUnBindWindow = obj.ayPyEwWIyCmkTn(hwnd)
    End Function

    Public Function EnableFakeActive(ByVal en As Integer) As Integer
        EnableFakeActive = obj.nSwocSTHQFwoz(en)
    End Function

    Public Function FindDataEx(ByVal hwnd As Integer,ByVal addr_range As String,ByVal data As String,ByVal steps As Integer,ByVal multi_thread As Integer,ByVal mode As Integer) As String
        FindDataEx = obj.VYPFkFKV(hwnd,addr_range,data,steps,multi_thread,mode)
    End Function

    Public Function FindShapeEx(ByVal x1 As Integer,ByVal y1 As Integer,ByVal x2 As Integer,ByVal y2 As Integer,ByVal offset_color As String,ByVal sim As Double,ByVal dir As Integer) As String
        FindShapeEx = obj.yUPkEDken(x1,y1,x2,y2,offset_color,sim,dir)
    End Function

    Public Function Assemble(ByVal asm_code As String,ByVal base_addr As Integer,ByVal is_upper As Integer) As String
        Assemble = obj.GMNvSMJcQqs(asm_code,base_addr,is_upper)
    End Function

    Public Function WriteFile(ByVal file_name As String,ByVal content As String) As Integer
        WriteFile = obj.mKbsvxXyE(file_name,content)
    End Function

    Public Function KeyPress(ByVal vk As Integer) As Integer
        KeyPress = obj.KgmsFSKjxsrt(vk)
    End Function

    Public Function FoobarSetTrans(ByVal hwnd As Integer,ByVal trans As Integer,ByVal color As String,ByVal sim As Double) As Integer
        FoobarSetTrans = obj.XlasN(hwnd,trans,color,sim)
    End Function

    Public Function ReadData(ByVal hwnd As Integer,ByVal addr As String,ByVal length As Integer) As String
        ReadData = obj.fFnzUZSUJ(hwnd,addr,length)
    End Function

    Public Function WriteDoubleAddr(ByVal hwnd As Integer,ByVal addr As Integer,ByVal v As Double) As Integer
        WriteDoubleAddr = obj.tUYXXvujWMFCAQ(hwnd,addr,v)
    End Function

    Public Function CreateFoobarRect(ByVal hwnd As Integer,ByVal x As Integer,ByVal y As Integer,ByVal w As Integer,ByVal h As Integer) As Integer
        CreateFoobarRect = obj.XPVJZYa(hwnd,x,y,w,h)
    End Function

    Public Function SetDict(ByVal index As Integer,ByVal dict_name As String) As Integer
        SetDict = obj.Ntzc(index,dict_name)
    End Function

    Public Function GetProcessInfo(ByVal pid As Integer) As String
        GetProcessInfo = obj.oHQlLywafft(pid)
    End Function

    Public Function EnterCri() As Integer
        EnterCri = obj.kJwKLjPZnWandji()
    End Function

    Public Function FindPicE(ByVal x1 As Integer,ByVal y1 As Integer,ByVal x2 As Integer,ByVal y2 As Integer,ByVal pic_name As String,ByVal delta_color As String,ByVal sim As Double,ByVal dir As Integer) As String
        FindPicE = obj.pDSZvL(x1,y1,x2,y2,pic_name,delta_color,sim,dir)
    End Function

    Public Function EnableRealKeypad(ByVal en As Integer) As Integer
        EnableRealKeypad = obj.ayvqKBSpDrcXP(en)
    End Function

    Public Function FaqIsPosted() As Integer
        FaqIsPosted = obj.DybvWhBYN()
    End Function

    Public Function WriteFloatAddr(ByVal hwnd As Integer,ByVal addr As Integer,ByVal v As Single) As Integer
        WriteFloatAddr = obj.DwDLyqpxELysZhI(hwnd,addr,v)
    End Function

    Public Function FoobarClearText(ByVal hwnd As Integer) As Integer
        FoobarClearText = obj.RRzfJkJDvAIAo(hwnd)
    End Function

    Public Function GetCursorShape() As String
        GetCursorShape = obj.uKuZnzdDKcHa()
    End Function

    Public Function FaqPost(ByVal server As String,ByVal handle As Integer,ByVal request_type As Integer,ByVal time_out As Integer) As Integer
        FaqPost = obj.oBCCKxSPNclB(server,handle,request_type,time_out)
    End Function

    Public Function GetWordResultPos(ByVal str As String,ByVal index As Integer,<Runtime.InteropServices.Out()> ByRef x As Object,<Runtime.InteropServices.Out()> ByRef y As Object) As Integer
        GetWordResultPos = obj.SUYRHgJ(str,index,x,y)
    End Function

    Public Function WriteIntAddr(ByVal hwnd As Integer,ByVal addr As Integer,ByVal tpe As Integer,ByVal v As Integer) As Integer
        WriteIntAddr = obj.lFmWCcBRr(hwnd,addr,tpe,v)
    End Function

    Public Function DisablePowerSave() As Integer
        DisablePowerSave = obj.qgPoRPmwqEZNly()
    End Function

    Public Function FindFloatEx(ByVal hwnd As Integer,ByVal addr_range As String,ByVal float_value_min As Single,ByVal float_value_max As Single,ByVal steps As Integer,ByVal multi_thread As Integer,ByVal mode As Integer) As String
        FindFloatEx = obj.bkNwbwoZYQ(hwnd,addr_range,float_value_min,float_value_max,steps,multi_thread,mode)
    End Function

    Public Function SetWindowState(ByVal hwnd As Integer,ByVal flag As Integer) As Integer
        SetWindowState = obj.UlLgRy(hwnd,flag)
    End Function

    Public Function FindWindowEx(ByVal parent As Integer,ByVal class_name As String,ByVal title_name As String) As Integer
        FindWindowEx = obj.eYmzDzqeG(parent,class_name,title_name)
    End Function

    Public Function IsBind(ByVal hwnd As Integer) As Integer
        IsBind = obj.LxfNGIxJFlzp(hwnd)
    End Function

    Public Function ReadFile(ByVal file_name As String) As String
        ReadFile = obj.gXvDFaeTMYMZ(file_name)
    End Function

    Public Function EnableGetColorByCapture(ByVal en As Integer) As Integer
        EnableGetColorByCapture = obj.hiYZHQdGdAH(en)
    End Function

    Public Function GetClientRect(ByVal hwnd As Integer,<Runtime.InteropServices.Out()> ByRef x1 As Object,<Runtime.InteropServices.Out()> ByRef y1 As Object,<Runtime.InteropServices.Out()> ByRef x2 As Object,<Runtime.InteropServices.Out()> ByRef y2 As Object) As Integer
        GetClientRect = obj.gxyQCdkpk(hwnd,x1,y1,x2,y2)
    End Function

    Public Function CheckFontSmooth() As Integer
        CheckFontSmooth = obj.omlr()
    End Function

    Public Function WriteInt(ByVal hwnd As Integer,ByVal addr As String,ByVal tpe As Integer,ByVal v As Integer) As Integer
        WriteInt = obj.dQuWzwwDk(hwnd,addr,tpe,v)
    End Function

    Public Function ActiveInputMethod(ByVal hwnd As Integer,ByVal id As String) As Integer
        ActiveInputMethod = obj.wVdFwSix(hwnd,id)
    End Function

    Public Function FindColorE(ByVal x1 As Integer,ByVal y1 As Integer,ByVal x2 As Integer,ByVal y2 As Integer,ByVal color As String,ByVal sim As Double,ByVal dir As Integer) As String
        FindColorE = obj.ZzXoLbIkgUuiVhF(x1,y1,x2,y2,color,sim,dir)
    End Function

    Public Function RegNoMac(ByVal code As String,ByVal ver As String) As Integer
        RegNoMac = obj.HmHiYRrK(code,ver)
    End Function

    Public Function FoobarUnlock(ByVal hwnd As Integer) As Integer
        FoobarUnlock = obj.ReoxL(hwnd)
    End Function

    Public Function GetColorNum(ByVal x1 As Integer,ByVal y1 As Integer,ByVal x2 As Integer,ByVal y2 As Integer,ByVal color As String,ByVal sim As Double) As Integer
        GetColorNum = obj.TphMb(x1,y1,x2,y2,color,sim)
    End Function

    Public Function OcrExOne(ByVal x1 As Integer,ByVal y1 As Integer,ByVal x2 As Integer,ByVal y2 As Integer,ByVal color As String,ByVal sim As Double) As String
        OcrExOne = obj.rDuXy(x1,y1,x2,y2,color,sim)
    End Function

    Public Function FoobarTextPrintDir(ByVal hwnd As Integer,ByVal dir As Integer) As Integer
        FoobarTextPrintDir = obj.lEDlyFNxCHgkcL(hwnd,dir)
    End Function

    Public Function ExcludePos(ByVal all_pos As String,ByVal tpe As Integer,ByVal x1 As Integer,ByVal y1 As Integer,ByVal x2 As Integer,ByVal y2 As Integer) As String
        ExcludePos = obj.WHWMhfLumbFcmsd(all_pos,tpe,x1,y1,x2,y2)
    End Function

    Public Function FreeProcessMemory(ByVal hwnd As Integer) As Integer
        FreeProcessMemory = obj.Ljnr(hwnd)
    End Function

    Public Function FindNearestPos(ByVal all_pos As String,ByVal tpe As Integer,ByVal x As Integer,ByVal y As Integer) As String
        FindNearestPos = obj.KdEEvuLKUtlfBT(all_pos,tpe,x,y)
    End Function

    Public Function SetWordGap(ByVal word_gap As Integer) As Integer
        SetWordGap = obj.JflEd(word_gap)
    End Function

    Public Function GetWindowTitle(ByVal hwnd As Integer) As String
        GetWindowTitle = obj.eqyCtxHyHtPE(hwnd)
    End Function

    Public Function FindStrFastE(ByVal x1 As Integer,ByVal y1 As Integer,ByVal x2 As Integer,ByVal y2 As Integer,ByVal str As String,ByVal color As String,ByVal sim As Double) As String
        FindStrFastE = obj.LQUs(x1,y1,x2,y2,str,color,sim)
    End Function

    Public Function RegExNoMac(ByVal code As String,ByVal ver As String,ByVal ip As String) As Integer
        RegExNoMac = obj.hYpzkDXvrH(code,ver,ip)
    End Function

    Public Function SetRowGapNoDict(ByVal row_gap As Integer) As Integer
        SetRowGapNoDict = obj.qaiV(row_gap)
    End Function

    Public Function SetExactOcr(ByVal exact_ocr As Integer) As Integer
        SetExactOcr = obj.IPutHnnfuadh(exact_ocr)
    End Function

    Public Function GetDict(ByVal index As Integer,ByVal font_index As Integer) As String
        GetDict = obj.xooCh(index,font_index)
    End Function

    Public Function GetScreenWidth() As Integer
        GetScreenWidth = obj.iyoYm()
    End Function

    Public Function SetDisplayInput(ByVal mode As String) As Integer
        SetDisplayInput = obj.XdaU(mode)
    End Function

    Public Function AsmClear() As Integer
        AsmClear = obj.zWqoxEzwdRpzu()
    End Function

    Public Function GetWindowRect(ByVal hwnd As Integer,<Runtime.InteropServices.Out()> ByRef x1 As Object,<Runtime.InteropServices.Out()> ByRef y1 As Object,<Runtime.InteropServices.Out()> ByRef x2 As Object,<Runtime.InteropServices.Out()> ByRef y2 As Object) As Integer
        GetWindowRect = obj.GPqv(hwnd,x1,y1,x2,y2)
    End Function

    Public Function GetSpecialWindow(ByVal flag As Integer) As Integer
        GetSpecialWindow = obj.xcqa(flag)
    End Function

    Public Function SetPath(ByVal path As String) As Integer
        SetPath = obj.WBITnZJs(path)
    End Function

    Public Function FindStrFastS(ByVal x1 As Integer,ByVal y1 As Integer,ByVal x2 As Integer,ByVal y2 As Integer,ByVal str As String,ByVal color As String,ByVal sim As Double,<Runtime.InteropServices.Out()> ByRef x As Object,<Runtime.InteropServices.Out()> ByRef y As Object) As String
        FindStrFastS = obj.vuRRE(x1,y1,x2,y2,str,color,sim,x,y)
    End Function

    Public Function GetBindWindow() As Integer
        GetBindWindow = obj.PZzvkwbIGgDR()
    End Function

    Public Function GetOsType() As Integer
        GetOsType = obj.BRaKImV()
    End Function

    Public Function FindStrWithFontEx(ByVal x1 As Integer,ByVal y1 As Integer,ByVal x2 As Integer,ByVal y2 As Integer,ByVal str As String,ByVal color As String,ByVal sim As Double,ByVal font_name As String,ByVal font_size As Integer,ByVal flag As Integer) As String
        FindStrWithFontEx = obj.CNZnrZrB(x1,y1,x2,y2,str,color,sim,font_name,font_size,flag)
    End Function

    Public Function GetModuleBaseAddr(ByVal hwnd As Integer,ByVal module_name As String) As Integer
        GetModuleBaseAddr = obj.LaLLEEbHxI(hwnd,module_name)
    End Function

    Public Function EnableBind(ByVal en As Integer) As Integer
        EnableBind = obj.MyPqQqyv(en)
    End Function

    Public Function KeyUp(ByVal vk As Integer) As Integer
        KeyUp = obj.KbYqgEv(vk)
    End Function

    Public Function RightUp() As Integer
        RightUp = obj.MblqFvJRRt()
    End Function

    Public Function AsmCall(ByVal hwnd As Integer,ByVal mode As Integer) As Integer
        AsmCall = obj.jVdLVLKNJTIFdP(hwnd,mode)
    End Function

    Public Function FoobarFillRect(ByVal hwnd As Integer,ByVal x1 As Integer,ByVal y1 As Integer,ByVal x2 As Integer,ByVal y2 As Integer,ByVal color As String) As Integer
        FoobarFillRect = obj.GIPNjgPonkufr(hwnd,x1,y1,x2,y2,color)
    End Function

    Public Function FreePic(ByVal pic_name As String) As Integer
        FreePic = obj.sHiesYFEp(pic_name)
    End Function

    Public Function FindStrEx(ByVal x1 As Integer,ByVal y1 As Integer,ByVal x2 As Integer,ByVal y2 As Integer,ByVal str As String,ByVal color As String,ByVal sim As Double) As String
        FindStrEx = obj.gmNQgaWFnNY(x1,y1,x2,y2,str,color,sim)
    End Function

    Public Function ReadFloat(ByVal hwnd As Integer,ByVal addr As String) As Single
        ReadFloat = obj.blzwi(hwnd,addr)
    End Function

    Public Function FindColorEx(ByVal x1 As Integer,ByVal y1 As Integer,ByVal x2 As Integer,ByVal y2 As Integer,ByVal color As String,ByVal sim As Double,ByVal dir As Integer) As String
        FindColorEx = obj.yCxrB(x1,y1,x2,y2,color,sim,dir)
    End Function

    Public Function FoobarUpdate(ByVal hwnd As Integer) As Integer
        FoobarUpdate = obj.ZxLIloEPhdWM(hwnd)
    End Function

    Public Function MoveToEx(ByVal x As Integer,ByVal y As Integer,ByVal w As Integer,ByVal h As Integer) As String
        MoveToEx = obj.FHMSNB(x,y,w,h)
    End Function

    Public Function MoveWindow(ByVal hwnd As Integer,ByVal x As Integer,ByVal y As Integer) As Integer
        MoveWindow = obj.gmKQYztJx(hwnd,x,y)
    End Function

    Public Function SetWordLineHeightNoDict(ByVal line_height As Integer) As Integer
        SetWordLineHeightNoDict = obj.zyviIzQk(line_height)
    End Function

    Public Function GetDictInfo(ByVal str As String,ByVal font_name As String,ByVal font_size As Integer,ByVal flag As Integer) As String
        GetDictInfo = obj.qXvjKJFIxw(str,font_name,font_size,flag)
    End Function

    Public Function SetWindowTransparent(ByVal hwnd As Integer,ByVal v As Integer) As Integer
        SetWindowTransparent = obj.bicyRgPXE(hwnd,v)
    End Function

    Public Function FindMultiColorE(ByVal x1 As Integer,ByVal y1 As Integer,ByVal x2 As Integer,ByVal y2 As Integer,ByVal first_color As String,ByVal offset_color As String,ByVal sim As Double,ByVal dir As Integer) As String
        FindMultiColorE = obj.ZCFjWyVPaYFI(x1,y1,x2,y2,first_color,offset_color,sim,dir)
    End Function

    Public Function ReadDoubleAddr(ByVal hwnd As Integer,ByVal addr As Integer) As Double
        ReadDoubleAddr = obj.zQPFBZ(hwnd,addr)
    End Function

    Public Function SendStringIme(ByVal str As String) As Integer
        SendStringIme = obj.EHJyXc(str)
    End Function

    Public Function TerminateProcess(ByVal pid As Integer) As Integer
        TerminateProcess = obj.vNTkdLjeyZJybTs(pid)
    End Function

    Public Function ReadFloatAddr(ByVal hwnd As Integer,ByVal addr As Integer) As Single
        ReadFloatAddr = obj.psXRYdVpISc(hwnd,addr)
    End Function

    Public Function BindWindowEx(ByVal hwnd As Integer,ByVal display As String,ByVal mouse As String,ByVal keypad As String,ByVal public_desc As String,ByVal mode As Integer) As Integer
        BindWindowEx = obj.xrsqu(hwnd,display,mouse,keypad,public_desc,mode)
    End Function

    Public Function GetNetTimeSafe() As String
        GetNetTimeSafe = obj.fTpGjGvX()
    End Function

    Public Function GetScreenDepth() As Integer
        GetScreenDepth = obj.ksVGR()
    End Function

    Public Function CreateFolder(ByVal folder_name As String) As Integer
        CreateFolder = obj.kVrkjUYpbvz(folder_name)
    End Function

    Public Function CopyFile(ByVal src_file As String,ByVal dst_file As String,ByVal over As Integer) As Integer
        CopyFile = obj.SuwjAlx(src_file,dst_file,over)
    End Function

    Public Function EnumWindow(ByVal parent As Integer,ByVal title As String,ByVal class_name As String,ByVal filter As Integer) As String
        EnumWindow = obj.eaDLdhbTzrTXq(parent,title,class_name,filter)
    End Function

    Public Function SetWordGapNoDict(ByVal word_gap As Integer) As Integer
        SetWordGapNoDict = obj.qKwokRd(word_gap)
    End Function

    Public Function SetMinRowGap(ByVal row_gap As Integer) As Integer
        SetMinRowGap = obj.TcmTbnkojeYVpJE(row_gap)
    End Function

    Public Function CmpColor(ByVal x As Integer,ByVal y As Integer,ByVal color As String,ByVal sim As Double) As Integer
        CmpColor = obj.rUYGF(x,y,color,sim)
    End Function

    Public Function RightClick() As Integer
        RightClick = obj.SKLe()
    End Function

    Public Function FindShape(ByVal x1 As Integer,ByVal y1 As Integer,ByVal x2 As Integer,ByVal y2 As Integer,ByVal offset_color As String,ByVal sim As Double,ByVal dir As Integer,<Runtime.InteropServices.Out()> ByRef x As Object,<Runtime.InteropServices.Out()> ByRef y As Object) As Integer
        FindShape = obj.tcWsEbUHUxF(x1,y1,x2,y2,offset_color,sim,dir,x,y)
    End Function

    Public Function FoobarDrawLine(ByVal hwnd As Integer,ByVal x1 As Integer,ByVal y1 As Integer,ByVal x2 As Integer,ByVal y2 As Integer,ByVal color As String,ByVal style As Integer,ByVal width As Integer) As Integer
        FoobarDrawLine = obj.KybJAXyBxFV(hwnd,x1,y1,x2,y2,color,style,width)
    End Function

    Public Function DisableFontSmooth() As Integer
        DisableFontSmooth = obj.AJPyyMapd()
    End Function

    Public Function EnableSpeedDx(ByVal en As Integer) As Integer
        EnableSpeedDx = obj.QuckjHuj(en)
    End Function

    Public Function LeftDoubleClick() As Integer
        LeftDoubleClick = obj.miLnDtGTR()
    End Function

    Public Function FindWindowByProcessId(ByVal process_id As Integer,ByVal class_name As String,ByVal title_name As String) As Integer
        FindWindowByProcessId = obj.uBvLiK(process_id,class_name,title_name)
    End Function

    Public Function WriteIniPwd(ByVal section As String,ByVal key As String,ByVal v As String,ByVal file_name As String,ByVal pwd As String) As Integer
        WriteIniPwd = obj.KqEnWbdGRMihew(section,key,v,file_name,pwd)
    End Function

    Public Function KeyPressChar(ByVal key_str As String) As Integer
        KeyPressChar = obj.HCdeKlYeKQ(key_str)
    End Function

    Public Function FaqCapture(ByVal x1 As Integer,ByVal y1 As Integer,ByVal x2 As Integer,ByVal y2 As Integer,ByVal quality As Integer,ByVal delay As Integer,ByVal time As Integer) As Integer
        FaqCapture = obj.WDfEZw(x1,y1,x2,y2,quality,delay,time)
    End Function

    Public Function FindPicEx(ByVal x1 As Integer,ByVal y1 As Integer,ByVal x2 As Integer,ByVal y2 As Integer,ByVal pic_name As String,ByVal delta_color As String,ByVal sim As Double,ByVal dir As Integer) As String
        FindPicEx = obj.ojbBmPSJU(x1,y1,x2,y2,pic_name,delta_color,sim,dir)
    End Function

    Public Function WriteStringAddr(ByVal hwnd As Integer,ByVal addr As Integer,ByVal tpe As Integer,ByVal v As String) As Integer
        WriteStringAddr = obj.PsEcMFwVpl(hwnd,addr,tpe,v)
    End Function

    Public Function EnableRealMouse(ByVal en As Integer,ByVal mousedelay As Integer,ByVal mousestep As Integer) As Integer
        EnableRealMouse = obj.dApmhiTpevWXunL(en,mousedelay,mousestep)
    End Function

    Public Function GetDmCount() As Integer
        GetDmCount = obj.KJpPLvG()
    End Function

    Public Function DecodeFile(ByVal file_name As String,ByVal pwd As String) As Integer
        DecodeFile = obj.JCMuNqUBWq(file_name,pwd)
    End Function

    Public Function EnumWindowByProcess(ByVal process_name As String,ByVal title As String,ByVal class_name As String,ByVal filter As Integer) As String
        EnumWindowByProcess = obj.UyHfyyrJU(process_name,title,class_name,filter)
    End Function

    Public Function FindPic(ByVal x1 As Integer,ByVal y1 As Integer,ByVal x2 As Integer,ByVal y2 As Integer,ByVal pic_name As String,ByVal delta_color As String,ByVal sim As Double,ByVal dir As Integer,<Runtime.InteropServices.Out()> ByRef x As Object,<Runtime.InteropServices.Out()> ByRef y As Object) As Integer
        FindPic = obj.SlcXeASunR(x1,y1,x2,y2,pic_name,delta_color,sim,dir,x,y)
    End Function

    Public Function MiddleClick() As Integer
        MiddleClick = obj.ZjHSMHvguuACy()
    End Function

    Public Function IsFileExist(ByVal file_name As String) As Integer
        IsFileExist = obj.DKYdmWy(file_name)
    End Function

    Public Function FindInt(ByVal hwnd As Integer,ByVal addr_range As String,ByVal int_value_min As Integer,ByVal int_value_max As Integer,ByVal tpe As Integer) As String
        FindInt = obj.CPHtLpoSoUtBUy(hwnd,addr_range,int_value_min,int_value_max,tpe)
    End Function

    Public Function FindData(ByVal hwnd As Integer,ByVal addr_range As String,ByVal data As String) As String
        FindData = obj.faITFNcLBUywa(hwnd,addr_range,data)
    End Function

    Public Function CheckUAC() As Integer
        CheckUAC = obj.UeLgaHZ()
    End Function

    Public Function FloatToData(ByVal float_value As Single) As String
        FloatToData = obj.qKLfXnPZPG(float_value)
    End Function

    Public Function GetNetTime() As String
        GetNetTime = obj.ergCRSBwpkQML()
    End Function

    Public Function GetAveRGB(ByVal x1 As Integer,ByVal y1 As Integer,ByVal x2 As Integer,ByVal y2 As Integer) As String
        GetAveRGB = obj.hUJsCaQ(x1,y1,x2,y2)
    End Function

    Public Function UnBindWindow() As Integer
        UnBindWindow = obj.XmJEdVtdD()
    End Function

    Public Function GetColorHSV(ByVal x As Integer,ByVal y As Integer) As String
        GetColorHSV = obj.fUfyR(x,y)
    End Function

    Public Function SetMemoryHwndAsProcessId(ByVal en As Integer) As Integer
        SetMemoryHwndAsProcessId = obj.LffwdiVgFM(en)
    End Function

    Public Function GetCursorShapeEx(ByVal tpe As Integer) As String
        GetCursorShapeEx = obj.mIpnVLs(tpe)
    End Function

    Public Function FindColorBlock(ByVal x1 As Integer,ByVal y1 As Integer,ByVal x2 As Integer,ByVal y2 As Integer,ByVal color As String,ByVal sim As Double,ByVal count As Integer,ByVal width As Integer,ByVal height As Integer,<Runtime.InteropServices.Out()> ByRef x As Object,<Runtime.InteropServices.Out()> ByRef y As Object) As Integer
        FindColorBlock = obj.daMzT(x1,y1,x2,y2,color,sim,count,width,height,x,y)
    End Function

    Public Function ReadInt(ByVal hwnd As Integer,ByVal addr As String,ByVal tpe As Integer) As Integer
        ReadInt = obj.DEhILPfzibJckp(hwnd,addr,tpe)
    End Function

    Public Function GetInfo(ByVal cmd As String,ByVal param As String) As String
        GetInfo = obj.pkjkWpVbGTRmy(cmd,param)
    End Function

    Public Function CreateFoobarEllipse(ByVal hwnd As Integer,ByVal x As Integer,ByVal y As Integer,ByVal w As Integer,ByVal h As Integer) As Integer
        CreateFoobarEllipse = obj.rlFovwRFp(hwnd,x,y,w,h)
    End Function

    Public Function FoobarDrawText(ByVal hwnd As Integer,ByVal x As Integer,ByVal y As Integer,ByVal w As Integer,ByVal h As Integer,ByVal text As String,ByVal color As String,ByVal align As Integer) As Integer
        FoobarDrawText = obj.BxzialYJI(hwnd,x,y,w,h,text,color,align)
    End Function

    Public Function GetForegroundFocus() As Integer
        GetForegroundFocus = obj.XJmw()
    End Function

    Public Function LeftClick() As Integer
        LeftClick = obj.fJZvEyWRIZEQlTw()
    End Function

    Public Function SetDictMem(ByVal index As Integer,ByVal addr As Integer,ByVal size As Integer) As Integer
        SetDictMem = obj.lnjv(index,addr,size)
    End Function

    Public Function EnumWindowSuper(ByVal spec1 As String,ByVal flag1 As Integer,ByVal type1 As Integer,ByVal spec2 As String,ByVal flag2 As Integer,ByVal type2 As Integer,ByVal sort As Integer) As String
        EnumWindowSuper = obj.BIHEoZKZjyRor(spec1,flag1,type1,spec2,flag2,type2,sort)
    End Function

    Public Function FindStrS(ByVal x1 As Integer,ByVal y1 As Integer,ByVal x2 As Integer,ByVal y2 As Integer,ByVal str As String,ByVal color As String,ByVal sim As Double,<Runtime.InteropServices.Out()> ByRef x As Object,<Runtime.InteropServices.Out()> ByRef y As Object) As String
        FindStrS = obj.bVlDicKemG(x1,y1,x2,y2,str,color,sim,x,y)
    End Function

    Public Function SetClientSize(ByVal hwnd As Integer,ByVal width As Integer,ByVal height As Integer) As Integer
        SetClientSize = obj.jajHPnY(hwnd,width,height)
    End Function

    Public Function AsmCode(ByVal base_addr As Integer) As String
        AsmCode = obj.AKZAnsPU(base_addr)
    End Function

    Public Function SelectDirectory() As String
        SelectDirectory = obj.vLgdAUSTzSssKZ()
    End Function

    Public Function GetCursorPos(<Runtime.InteropServices.Out()> ByRef x As Object,<Runtime.InteropServices.Out()> ByRef y As Object) As Integer
        GetCursorPos = obj.XuoXc(x,y)
    End Function

    Public Function KeyDown(ByVal vk As Integer) As Integer
        KeyDown = obj.xMQurhaXFlCNQZD(vk)
    End Function

    Public Function OcrEx(ByVal x1 As Integer,ByVal y1 As Integer,ByVal x2 As Integer,ByVal y2 As Integer,ByVal color As String,ByVal sim As Double) As String
        OcrEx = obj.QlHGNd(x1,y1,x2,y2,color,sim)
    End Function

    Public Function DoubleToData(ByVal double_value As Double) As String
        DoubleToData = obj.KMRt(double_value)
    End Function

    Public Function AddDict(ByVal index As Integer,ByVal dict_info As String) As Integer
        AddDict = obj.zcJxmzDMSH(index,dict_info)
    End Function

    Public Function FindStrWithFont(ByVal x1 As Integer,ByVal y1 As Integer,ByVal x2 As Integer,ByVal y2 As Integer,ByVal str As String,ByVal color As String,ByVal sim As Double,ByVal font_name As String,ByVal font_size As Integer,ByVal flag As Integer,<Runtime.InteropServices.Out()> ByRef x As Object,<Runtime.InteropServices.Out()> ByRef y As Object) As Integer
        FindStrWithFont = obj.SRsgaozovRhY(x1,y1,x2,y2,str,color,sim,font_name,font_size,flag,x,y)
    End Function

    Public Function GetMachineCode() As String
        GetMachineCode = obj.YHzyoIZrsYPmQSe()
    End Function

    Public Function FindString(ByVal hwnd As Integer,ByVal addr_range As String,ByVal string_value As String,ByVal tpe As Integer) As String
        FindString = obj.pRgagcvyyBTyVEK(hwnd,addr_range,string_value,tpe)
    End Function

    Public Function LoadPicByte(ByVal addr As Integer,ByVal size As Integer,ByVal name As String) As Integer
        LoadPicByte = obj.SiZQZa(addr,size,name)
    End Function

    Public Function MatchPicName(ByVal pic_name As String) As String
        MatchPicName = obj.GHdnDlPyaW(pic_name)
    End Function

    Public Function OcrInFile(ByVal x1 As Integer,ByVal y1 As Integer,ByVal x2 As Integer,ByVal y2 As Integer,ByVal pic_name As String,ByVal color As String,ByVal sim As Double) As String
        OcrInFile = obj.VvaFJGWektvHmtf(x1,y1,x2,y2,pic_name,color,sim)
    End Function

    Public Function GetWords(ByVal x1 As Integer,ByVal y1 As Integer,ByVal x2 As Integer,ByVal y2 As Integer,ByVal color As String,ByVal sim As Double) As String
        GetWords = obj.EmiZAZuMpVdY(x1,y1,x2,y2,color,sim)
    End Function

    Public Function FaqGetSize(ByVal handle As Integer) As Integer
        FaqGetSize = obj.guqhbZMy(handle)
    End Function

    Public Function FindPicMemEx(ByVal x1 As Integer,ByVal y1 As Integer,ByVal x2 As Integer,ByVal y2 As Integer,ByVal pic_info As String,ByVal delta_color As String,ByVal sim As Double,ByVal dir As Integer) As String
        FindPicMemEx = obj.XKbY(x1,y1,x2,y2,pic_info,delta_color,sim,dir)
    End Function

    Public Function SetClipboard(ByVal data As String) As Integer
        SetClipboard = obj.bigV(data)
    End Function

    Public Function StopAudio(ByVal id As Integer) As Integer
        StopAudio = obj.yiypDGY(id)
    End Function

    Public Function FoobarTextLineGap(ByVal hwnd As Integer,ByVal gap As Integer) As Integer
        FoobarTextLineGap = obj.NoUQBMqJmKuCBuJ(hwnd,gap)
    End Function

    Public Function FaqSend(ByVal server As String,ByVal handle As Integer,ByVal request_type As Integer,ByVal time_out As Integer) As String
        FaqSend = obj.lpoBFfIfsN(server,handle,request_type,time_out)
    End Function

    Public Function GetCommandLine(ByVal hwnd As Integer) As String
        GetCommandLine = obj.welCE(hwnd)
    End Function

    Public Function GetDir(ByVal tpe As Integer) As String
        GetDir = obj.jclpfjgBvW(tpe)
    End Function

    Public Function GetResultPos(ByVal str As String,ByVal index As Integer,<Runtime.InteropServices.Out()> ByRef x As Object,<Runtime.InteropServices.Out()> ByRef y As Object) As Integer
        GetResultPos = obj.cXdDE(str,index,x,y)
    End Function

    Public Function FoobarDrawPic(ByVal hwnd As Integer,ByVal x As Integer,ByVal y As Integer,ByVal pic As String,ByVal trans_color As String) As Integer
        FoobarDrawPic = obj.SjGK(hwnd,x,y,pic,trans_color)
    End Function

    Public Function GetPointWindow(ByVal x As Integer,ByVal y As Integer) As Integer
        GetPointWindow = obj.nzdMyJTXAehJ(x,y)
    End Function

    Public Function FindStrFastEx(ByVal x1 As Integer,ByVal y1 As Integer,ByVal x2 As Integer,ByVal y2 As Integer,ByVal str As String,ByVal color As String,ByVal sim As Double) As String
        FindStrFastEx = obj.vjaHzYQYbp(x1,y1,x2,y2,str,color,sim)
    End Function

    Public Function RightDown() As Integer
        RightDown = obj.eNeLhJWK()
    End Function

    Public Function FindInputMethod(ByVal id As String) As Integer
        FindInputMethod = obj.EyciUYHbZN(id)
    End Function

    Public Function SetKeypadDelay(ByVal tpe As String,ByVal delay As Integer) As Integer
        SetKeypadDelay = obj.zrfUP(tpe,delay)
    End Function

    Public Function EnumIniSection(ByVal file_name As String) As String
        EnumIniSection = obj.jRlwUBSTSn(file_name)
    End Function

    Public Function FindStrFastExS(ByVal x1 As Integer,ByVal y1 As Integer,ByVal x2 As Integer,ByVal y2 As Integer,ByVal str As String,ByVal color As String,ByVal sim As Double) As String
        FindStrFastExS = obj.TEumIEdQCpXwAV(x1,y1,x2,y2,str,color,sim)
    End Function

    Public Function WriteString(ByVal hwnd As Integer,ByVal addr As String,ByVal tpe As Integer,ByVal v As String) As Integer
        WriteString = obj.SZvACXIyDGwP(hwnd,addr,tpe,v)
    End Function

    Public Function CheckInputMethod(ByVal hwnd As Integer,ByVal id As String) As Integer
        CheckInputMethod = obj.clfbeYChdFClgcR(hwnd,id)
    End Function

    Public Function GetDisplayInfo() As String
        GetDisplayInfo = obj.DEINWxwLzU()
    End Function

    Public Function LockInput(ByVal locks As Integer) As Integer
        LockInput = obj.vSGWsBppSJvphN(locks)
    End Function

    Public Function AsmAdd(ByVal asm_ins As String) As Integer
        AsmAdd = obj.QtgBVifk(asm_ins)
    End Function

    Public Function FoobarPrintText(ByVal hwnd As Integer,ByVal text As String,ByVal color As String) As Integer
        FoobarPrintText = obj.IKiFgrYPnqkvUA(hwnd,text,color)
    End Function

    Public Function MoveFile(ByVal src_file As String,ByVal dst_file As String) As Integer
        MoveFile = obj.Myhs(src_file,dst_file)
    End Function

    Public Function FindColorBlockEx(ByVal x1 As Integer,ByVal y1 As Integer,ByVal x2 As Integer,ByVal y2 As Integer,ByVal color As String,ByVal sim As Double,ByVal count As Integer,ByVal width As Integer,ByVal height As Integer) As String
        FindColorBlockEx = obj.ZKQRYzfr(x1,y1,x2,y2,color,sim,count,width,height)
    End Function

    Public Function Delays(ByVal min_s As Integer,ByVal max_s As Integer) As Integer
        Delays = obj.UJmKnn(min_s,max_s)
    End Function

    Public Function GetColor(ByVal x As Integer,ByVal y As Integer) As String
        GetColor = obj.JdkNgqAQAeFYdj(x,y)
    End Function

    Public Function SaveDict(ByVal index As Integer,ByVal file_name As String) As Integer
        SaveDict = obj.UaRLJWpP(index,file_name)
    End Function

    Public Function FindMultiColorEx(ByVal x1 As Integer,ByVal y1 As Integer,ByVal x2 As Integer,ByVal y2 As Integer,ByVal first_color As String,ByVal offset_color As String,ByVal sim As Double,ByVal dir As Integer) As String
        FindMultiColorEx = obj.ZqYEhyMHUAb(x1,y1,x2,y2,first_color,offset_color,sim,dir)
    End Function

    Public Function ReadString(ByVal hwnd As Integer,ByVal addr As String,ByVal tpe As Integer,ByVal length As Integer) As String
        ReadString = obj.UIjlVnuKB(hwnd,addr,tpe,length)
    End Function

    Public Function GetLastError() As Integer
        GetLastError = obj.SqgwkiDIVPg()
    End Function

    Public Function FaqCancel() As Integer
        FaqCancel = obj.NKpZRJQbX()
    End Function

    Public Function GetWordsNoDict(ByVal x1 As Integer,ByVal y1 As Integer,ByVal x2 As Integer,ByVal y2 As Integer,ByVal color As String) As String
        GetWordsNoDict = obj.wPJYIfUXuoQ(x1,y1,x2,y2,color)
    End Function

    Public Function GetColorBGR(ByVal x As Integer,ByVal y As Integer) As String
        GetColorBGR = obj.LNeMiIglljlLEf(x,y)
    End Function

    Public Function LockMouseRect(ByVal x1 As Integer,ByVal y1 As Integer,ByVal x2 As Integer,ByVal y2 As Integer) As Integer
        LockMouseRect = obj.WgPsAHBS(x1,y1,x2,y2)
    End Function

    Public Function GetWindowState(ByVal hwnd As Integer,ByVal flag As Integer) As Integer
        GetWindowState = obj.HXIoNuNy(hwnd,flag)
    End Function

    Public Function GetID() As Integer
        GetID = obj.tIKno()
    End Function

    Public Function VirtualFreeEx(ByVal hwnd As Integer,ByVal addr As Integer) As Integer
        VirtualFreeEx = obj.rnTejrhYZUaCmj(hwnd,addr)
    End Function

    Public Function FoobarStopGif(ByVal hwnd As Integer,ByVal x As Integer,ByVal y As Integer,ByVal pic_name As String) As Integer
        FoobarStopGif = obj.ryZmsyvKiZbPh(hwnd,x,y,pic_name)
    End Function

    Public Function GetAveHSV(ByVal x1 As Integer,ByVal y1 As Integer,ByVal x2 As Integer,ByVal y2 As Integer) As String
        GetAveHSV = obj.MFMfbP(x1,y1,x2,y2)
    End Function

    Public Function FindWindowByProcess(ByVal process_name As String,ByVal class_name As String,ByVal title_name As String) As Integer
        FindWindowByProcess = obj.bVQRqxMSC(process_name,class_name,title_name)
    End Function

    Public Function GetNetTimeByIp(ByVal ip As String) As String
        GetNetTimeByIp = obj.mtLYbkYKZReVAdn(ip)
    End Function

    Public Function SetWindowSize(ByVal hwnd As Integer,ByVal width As Integer,ByVal height As Integer) As Integer
        SetWindowSize = obj.nPRfFJiuDCpQmb(hwnd,width,height)
    End Function

    Public Function EnableKeypadPatch(ByVal en As Integer) As Integer
        EnableKeypadPatch = obj.AIhi(en)
    End Function

    Public Function EnumIniSectionPwd(ByVal file_name As String,ByVal pwd As String) As String
        EnumIniSectionPwd = obj.hxywhGVsanuKM(file_name,pwd)
    End Function

    Public Function EnableDisplayDebug(ByVal enable_debug As Integer) As Integer
        EnableDisplayDebug = obj.oZJErJCoJuf(enable_debug)
    End Function

    Public Function MiddleUp() As Integer
        MiddleUp = obj.BLYvlj()
    End Function

    Public Function GetMousePointWindow() As Integer
        GetMousePointWindow = obj.HGUiMuDPYBcJ()
    End Function

    Public Function EnablePicCache(ByVal en As Integer) As Integer
        EnablePicCache = obj.SgeIYKM(en)
    End Function

    Public Function LeftUp() As Integer
        LeftUp = obj.YJufp()
    End Function

    Public Function GetBasePath() As String
        GetBasePath = obj.SsdMLvLnHgBgHaS()
    End Function

    Public Function ReadDataAddr(ByVal hwnd As Integer,ByVal addr As Integer,ByVal length As Integer) As String
        ReadDataAddr = obj.njoYkRlFqzm(hwnd,addr,length)
    End Function

    Public Function GetDictCount(ByVal index As Integer) As Integer
        GetDictCount = obj.EMXajIXsfwLTkFR(index)
    End Function

    Public Function KeyPressStr(ByVal key_str As String,ByVal delay As Integer) As Integer
        KeyPressStr = obj.EDff(key_str,delay)
    End Function

    Public Function FindWindowSuper(ByVal spec1 As String,ByVal flag1 As Integer,ByVal type1 As Integer,ByVal spec2 As String,ByVal flag2 As Integer,ByVal type2 As Integer) As Integer
        FindWindowSuper = obj.upcieUhvpqNKpkv(spec1,flag1,type1,spec2,flag2,type2)
    End Function

    Public Function FindStrFast(ByVal x1 As Integer,ByVal y1 As Integer,ByVal x2 As Integer,ByVal y2 As Integer,ByVal str As String,ByVal color As String,ByVal sim As Double,<Runtime.InteropServices.Out()> ByRef x As Object,<Runtime.InteropServices.Out()> ByRef y As Object) As Integer
        FindStrFast = obj.aSBbRDsum(x1,y1,x2,y2,str,color,sim,x,y)
    End Function

    Public Function MoveR(ByVal rx As Integer,ByVal ry As Integer) As Integer
        MoveR = obj.MvkYcKkaK(rx,ry)
    End Function

    Public Function FindStringEx(ByVal hwnd As Integer,ByVal addr_range As String,ByVal string_value As String,ByVal tpe As Integer,ByVal steps As Integer,ByVal multi_thread As Integer,ByVal mode As Integer) As String
        FindStringEx = obj.IVfIPplxGAodwg(hwnd,addr_range,string_value,tpe,steps,multi_thread,mode)
    End Function

    Public Function Ocr(ByVal x1 As Integer,ByVal y1 As Integer,ByVal x2 As Integer,ByVal y2 As Integer,ByVal color As String,ByVal sim As Double) As String
        Ocr = obj.eXfcLqxTUz(x1,y1,x2,y2,color,sim)
    End Function

    Public Function DeleteIni(ByVal section As String,ByVal key As String,ByVal file_name As String) As Integer
        DeleteIni = obj.Imll(section,key,file_name)
    End Function

    Public Function GetScreenHeight() As Integer
        GetScreenHeight = obj.CSYeWtE()
    End Function

    Public Function MoveTo(ByVal x As Integer,ByVal y As Integer) As Integer
        MoveTo = obj.GzsJWDV(x,y)
    End Function

    Public Function VirtualAllocEx(ByVal hwnd As Integer,ByVal addr As Integer,ByVal size As Integer,ByVal tpe As Integer) As Integer
        VirtualAllocEx = obj.cmGEVSgaXNz(hwnd,addr,size,tpe)
    End Function

    Public Function FindMultiColor(ByVal x1 As Integer,ByVal y1 As Integer,ByVal x2 As Integer,ByVal y2 As Integer,ByVal first_color As String,ByVal offset_color As String,ByVal sim As Double,ByVal dir As Integer,<Runtime.InteropServices.Out()> ByRef x As Object,<Runtime.InteropServices.Out()> ByRef y As Object) As Integer
        FindMultiColor = obj.AYMNoCiT(x1,y1,x2,y2,first_color,offset_color,sim,dir,x,y)
    End Function

    Public Function RegEx(ByVal code As String,ByVal ver As String,ByVal ip As String) As Integer
        RegEx = obj.EutEhYkgkk(code,ver,ip)
    End Function

    Public Function SendStringIme2(ByVal hwnd As Integer,ByVal str As String,ByVal mode As Integer) As Integer
        SendStringIme2 = obj.ACENnVm(hwnd,str,mode)
    End Function

    Public Function FoobarSetSave(ByVal hwnd As Integer,ByVal file_name As String,ByVal en As Integer,ByVal header As String) As Integer
        FoobarSetSave = obj.zokmXjFVe(hwnd,file_name,en,header)
    End Function

    Public Function FindWindow(ByVal class_name As String,ByVal title_name As String) As Integer
        FindWindow = obj.byVFenwjx(class_name,title_name)
    End Function

    Public Function GetResultCount(ByVal str As String) As Integer
        GetResultCount = obj.hYwzRYjQmVEsb(str)
    End Function

    Public Function DmGuard(ByVal en As Integer,ByVal tpe As String) As Integer
        DmGuard = obj.rAnMaNrAMWWvE(en,tpe)
    End Function

    Public Function EnumProcess(ByVal name As String) As String
        EnumProcess = obj.fTZrkZHiPuR(name)
    End Function

    Public Function SetUAC(ByVal uac As Integer) As Integer
        SetUAC = obj.EQsbofgsLDoExFu(uac)
    End Function

    Public Function SetShowErrorMsg(ByVal show As Integer) As Integer
        SetShowErrorMsg = obj.yYWb(show)
    End Function

    Public Function Ver() As String
        Ver = obj.qUJpaMHKEF()
    End Function

    Public Function GetCursorSpot() As String
        GetCursorSpot = obj.sSPtDhfs()
    End Function

    Public Function BindWindow(ByVal hwnd As Integer,ByVal display As String,ByVal mouse As String,ByVal keypad As String,ByVal mode As Integer) As Integer
        BindWindow = obj.PfiriFtqCYyCBuY(hwnd,display,mouse,keypad,mode)
    End Function

    Public Function Beep(ByVal fre As Integer,ByVal delay As Integer) As Integer
        Beep = obj.HaSVGEHxt(fre,delay)
    End Function

    Public Function SendPaste(ByVal hwnd As Integer) As Integer
        SendPaste = obj.mufd(hwnd)
    End Function

    Public Function DeleteFolder(ByVal folder_name As String) As Integer
        DeleteFolder = obj.dCnRTug(folder_name)
    End Function

    Public Function CapturePng(ByVal x1 As Integer,ByVal y1 As Integer,ByVal x2 As Integer,ByVal y2 As Integer,ByVal file_name As String) As Integer
        CapturePng = obj.pNUNMfVFVqxYr(x1,y1,x2,y2,file_name)
    End Function

    Public Function EncodeFile(ByVal file_name As String,ByVal pwd As String) As Integer
        EncodeFile = obj.NldgdfDuEkXrB(file_name,pwd)
    End Function

    Public Function FindColor(ByVal x1 As Integer,ByVal y1 As Integer,ByVal x2 As Integer,ByVal y2 As Integer,ByVal color As String,ByVal sim As Double,ByVal dir As Integer,<Runtime.InteropServices.Out()> ByRef x As Object,<Runtime.InteropServices.Out()> ByRef y As Object) As Integer
        FindColor = obj.SNJhiclmAcXsSHf(x1,y1,x2,y2,color,sim,dir,x,y)
    End Function

    Public Function SetScreen(ByVal width As Integer,ByVal height As Integer,ByVal depth As Integer) As Integer
        SetScreen = obj.NRRwSYR(width,height,depth)
    End Function

    Public Function FindPicExS(ByVal x1 As Integer,ByVal y1 As Integer,ByVal x2 As Integer,ByVal y2 As Integer,ByVal pic_name As String,ByVal delta_color As String,ByVal sim As Double,ByVal dir As Integer) As String
        FindPicExS = obj.SefJNYBbK(x1,y1,x2,y2,pic_name,delta_color,sim,dir)
    End Function

    Public Function SetDictPwd(ByVal pwd As String) As Integer
        SetDictPwd = obj.SeJi(pwd)
    End Function

    Public Function GetWindowProcessPath(ByVal hwnd As Integer) As String
        GetWindowProcessPath = obj.GNjfD(hwnd)
    End Function

    Public Function GetClipboard() As String
        GetClipboard = obj.KbCslAehhfmv()
    End Function

    Public Function StringToData(ByVal string_value As String,ByVal tpe As Integer) As String
        StringToData = obj.lEdyJCsTwcQ(string_value,tpe)
    End Function

    Public Function EnableFontSmooth() As Integer
        EnableFontSmooth = obj.mvsvbzqE()
    End Function

    Public Function FoobarLock(ByVal hwnd As Integer) As Integer
        FoobarLock = obj.WZegdthZduoC(hwnd)
    End Function

    Public Function SetAero(ByVal en As Integer) As Integer
        SetAero = obj.ZWas(en)
    End Function

    Public Function OpenProcess(ByVal pid As Integer) As Integer
        OpenProcess = obj.gQNdiL(pid)
    End Function

    Public Function SortPosDistance(ByVal all_pos As String,ByVal tpe As Integer,ByVal x As Integer,ByVal y As Integer) As String
        SortPosDistance = obj.BidtDg(all_pos,tpe,x,y)
    End Function

    Public Function FindPicMemE(ByVal x1 As Integer,ByVal y1 As Integer,ByVal x2 As Integer,ByVal y2 As Integer,ByVal pic_info As String,ByVal delta_color As String,ByVal sim As Double,ByVal dir As Integer) As String
        FindPicMemE = obj.mgZzK(x1,y1,x2,y2,pic_info,delta_color,sim,dir)
    End Function

    Public Function DownCpu(ByVal rate As Integer) As Integer
        DownCpu = obj.gcebHzv(rate)
    End Function

    Public Function FindFloat(ByVal hwnd As Integer,ByVal addr_range As String,ByVal float_value_min As Single,ByVal float_value_max As Single) As String
        FindFloat = obj.mbynhtIbbJM(hwnd,addr_range,float_value_min,float_value_max)
    End Function

    Public Function ClearDict(ByVal index As Integer) As Integer
        ClearDict = obj.wxQX(index)
    End Function

    Public Function SetColGapNoDict(ByVal col_gap As Integer) As Integer
        SetColGapNoDict = obj.AEzUQYsaqDMDovN(col_gap)
    End Function

    Public Function EnumWindowByProcessId(ByVal pid As Integer,ByVal title As String,ByVal class_name As String,ByVal filter As Integer) As String
        EnumWindowByProcessId = obj.nachX(pid,title,class_name,filter)
    End Function

    Public Function SetDisplayDelay(ByVal t As Integer) As Integer
        SetDisplayDelay = obj.nFlrNapnQvtQGwX(t)
    End Function

    Public Function GetWindowProcessId(ByVal hwnd As Integer) As Integer
        GetWindowProcessId = obj.qFCS(hwnd)
    End Function

    Public Function DownloadFile(ByVal url As String,ByVal save_file As String,ByVal timeout As Integer) As Integer
        DownloadFile = obj.AQFIdCViF(url,save_file,timeout)
    End Function

    Public Function WaitKey(ByVal key_code As Integer,ByVal time_out As Integer) As Integer
        WaitKey = obj.jKKgx(key_code,time_out)
    End Function

    Public Function SendString(ByVal hwnd As Integer,ByVal str As String) As Integer
        SendString = obj.mBUQhKmbnrr(hwnd,str)
    End Function

    Public Function SwitchBindWindow(ByVal hwnd As Integer) As Integer
        SwitchBindWindow = obj.rczIFfjL(hwnd)
    End Function

    Public Function FindMulColor(ByVal x1 As Integer,ByVal y1 As Integer,ByVal x2 As Integer,ByVal y2 As Integer,ByVal color As String,ByVal sim As Double) As Integer
        FindMulColor = obj.gILcskv(x1,y1,x2,y2,color,sim)
    End Function

    Public Function Is64Bit() As Integer
        Is64Bit = obj.LSbgw()
    End Function

    Public Function CapturePre(ByVal file_name As String) As Integer
        CapturePre = obj.eKqtaAyuPMgHJNo(file_name)
    End Function

    Public Function IsDisplayDead(ByVal x1 As Integer,ByVal y1 As Integer,ByVal x2 As Integer,ByVal y2 As Integer,ByVal t As Integer) As Integer
        IsDisplayDead = obj.WsskdVCRhJCKq(x1,y1,x2,y2,t)
    End Function

    Public Function ReadIniPwd(ByVal section As String,ByVal key As String,ByVal file_name As String,ByVal pwd As String) As String
        ReadIniPwd = obj.WZyRiKmFRCBcqlW(section,key,file_name,pwd)
    End Function

    Public Function FindStrExS(ByVal x1 As Integer,ByVal y1 As Integer,ByVal x2 As Integer,ByVal y2 As Integer,ByVal str As String,ByVal color As String,ByVal sim As Double) As String
        FindStrExS = obj.cLJkwfXWnHaSnkH(x1,y1,x2,y2,str,color,sim)
    End Function

    Public Function SetSimMode(ByVal mode As Integer) As Integer
        SetSimMode = obj.vYAmRakzN(mode)
    End Function

    Public Function FindDoubleEx(ByVal hwnd As Integer,ByVal addr_range As String,ByVal double_value_min As Double,ByVal double_value_max As Double,ByVal steps As Integer,ByVal multi_thread As Integer,ByVal mode As Integer) As String
        FindDoubleEx = obj.LsSkWa(hwnd,addr_range,double_value_min,double_value_max,steps,multi_thread,mode)
    End Function

    Public Function AppendPicAddr(ByVal pic_info As String,ByVal addr As Integer,ByVal size As Integer) As String
        AppendPicAddr = obj.FcCKlZwwDmZTCB(pic_info,addr,size)
    End Function

    Public Function IntToData(ByVal int_value As Integer,ByVal tpe As Integer) As String
        IntToData = obj.fPWkizvspRH(int_value,tpe)
    End Function

    Public Function WriteDataAddr(ByVal hwnd As Integer,ByVal addr As Integer,ByVal data As String) As Integer
        WriteDataAddr = obj.RenAGB(hwnd,addr,data)
    End Function

    Public Function FindShapeE(ByVal x1 As Integer,ByVal y1 As Integer,ByVal x2 As Integer,ByVal y2 As Integer,ByVal offset_color As String,ByVal sim As Double,ByVal dir As Integer) As String
        FindShapeE = obj.SsPNlnAfv(x1,y1,x2,y2,offset_color,sim,dir)
    End Function

    Public Function ImageToBmp(ByVal pic_name As String,ByVal bmp_name As String) As Integer
        ImageToBmp = obj.JyJIDktXoInGJZ(pic_name,bmp_name)
    End Function

    Public Function SetWindowText(ByVal hwnd As Integer,ByVal text As String) As Integer
        SetWindowText = obj.sBsNCdgTSp(hwnd,text)
    End Function

    Public Function RunApp(ByVal path As String,ByVal mode As Integer) As Integer
        RunApp = obj.mzvDUtlXzvQqU(path,mode)
    End Function

    Public Function SelectFile() As String
        SelectFile = obj.jcSDTMYPWIDbfa()
    End Function

    Public Function DeleteIniPwd(ByVal section As String,ByVal key As String,ByVal file_name As String,ByVal pwd As String) As Integer
        DeleteIniPwd = obj.FGNGamzGqVdg(section,key,file_name,pwd)
    End Function

    Public Function SetWordLineHeight(ByVal line_height As Integer) As Integer
        SetWordLineHeight = obj.RqEejgtnUoAC(line_height)
    End Function

    Public Function ReadIni(ByVal section As String,ByVal key As String,ByVal file_name As String) As String
        ReadIni = obj.fxvAUPkbn(section,key,file_name)
    End Function

    Public Function KeyUpChar(ByVal key_str As String) As Integer
        KeyUpChar = obj.SgNKy(key_str)
    End Function

    Public Function GetScreenData(ByVal x1 As Integer,ByVal y1 As Integer,ByVal x2 As Integer,ByVal y2 As Integer) As Integer
        GetScreenData = obj.ClixUKxpoJSC(x1,y1,x2,y2)
    End Function

    Public Function MiddleDown() As Integer
        MiddleDown = obj.uELkqVUjBCupw()
    End Function

    Public Function GetDiskSerial() As String
        GetDiskSerial = obj.Vrrfawg()
    End Function

    Public Function RGB2BGR(ByVal rgb_color As String) As String
        RGB2BGR = obj.oMQHiZBKJ(rgb_color)
    End Function

    Public Function LeaveCri() As Integer
        LeaveCri = obj.lwfkQQEw()
    End Function

End Class
