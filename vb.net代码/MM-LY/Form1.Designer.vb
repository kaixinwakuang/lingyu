﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class mm_ly
    Inherits System.Windows.Forms.Form

    'Form 重写 Dispose，以清理组件列表。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Windows 窗体设计器所必需的
    Private components As System.ComponentModel.IContainer

    '注意: 以下过程是 Windows 窗体设计器所必需的
    '可以使用 Windows 窗体设计器修改它。
    '不要使用代码编辑器修改它。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnStart = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtDatiIp = New System.Windows.Forms.TextBox()
        Me.txtDatingSn = New System.Windows.Forms.TextBox()
        Me.lblNeiwang = New System.Windows.Forms.Label()
        Me.lblSn = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.btnIdPath = New System.Windows.Forms.Button()
        Me.btnGamePath = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtIdPath = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtGamePath = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.ckbLog = New System.Windows.Forms.CheckBox()
        Me.OpenFileDialogGamePath = New System.Windows.Forms.OpenFileDialog()
        Me.OpenFileDialogIdPath = New System.Windows.Forms.OpenFileDialog()
        Me.btnSaveini = New System.Windows.Forms.Button()
        Me.btnTest = New System.Windows.Forms.Button()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.dgwAccount = New System.Windows.Forms.DataGridView()
        Me.账户名 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.姓名 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.身份证 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.btnLoadAccount = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        CType(Me.dgwAccount, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnStart
        '
        Me.btnStart.Location = New System.Drawing.Point(685, 225)
        Me.btnStart.Name = "btnStart"
        Me.btnStart.Size = New System.Drawing.Size(75, 23)
        Me.btnStart.TabIndex = 0
        Me.btnStart.Text = "开始"
        Me.btnStart.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.txtDatiIp)
        Me.GroupBox1.Controls.Add(Me.txtDatingSn)
        Me.GroupBox1.Controls.Add(Me.lblNeiwang)
        Me.GroupBox1.Controls.Add(Me.lblSn)
        Me.GroupBox1.Location = New System.Drawing.Point(423, 11)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(338, 137)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "答题(91yzm)"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.ForeColor = System.Drawing.Color.Red
        Me.Label4.Location = New System.Drawing.Point(17, 100)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(209, 12)
        Me.Label4.TabIndex = 7
        Me.Label4.Text = "注意: 账号密码留空, 则使用内网答题"
        '
        'txtDatiIp
        '
        Me.txtDatiIp.Location = New System.Drawing.Point(76, 64)
        Me.txtDatiIp.Name = "txtDatiIp"
        Me.txtDatiIp.Size = New System.Drawing.Size(215, 21)
        Me.txtDatiIp.TabIndex = 3
        '
        'txtDatingSn
        '
        Me.txtDatingSn.Location = New System.Drawing.Point(76, 30)
        Me.txtDatingSn.Name = "txtDatingSn"
        Me.txtDatingSn.Size = New System.Drawing.Size(215, 21)
        Me.txtDatingSn.TabIndex = 2
        '
        'lblNeiwang
        '
        Me.lblNeiwang.AutoSize = True
        Me.lblNeiwang.Location = New System.Drawing.Point(17, 70)
        Me.lblNeiwang.Name = "lblNeiwang"
        Me.lblNeiwang.Size = New System.Drawing.Size(53, 12)
        Me.lblNeiwang.TabIndex = 1
        Me.lblNeiwang.Text = "内网答题"
        '
        'lblSn
        '
        Me.lblSn.AutoSize = True
        Me.lblSn.Location = New System.Drawing.Point(17, 33)
        Me.lblSn.Name = "lblSn"
        Me.lblSn.Size = New System.Drawing.Size(53, 12)
        Me.lblSn.TabIndex = 0
        Me.lblSn.Text = "91密码串"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.btnIdPath)
        Me.GroupBox2.Controls.Add(Me.btnGamePath)
        Me.GroupBox2.Controls.Add(Me.Label3)
        Me.GroupBox2.Controls.Add(Me.txtIdPath)
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.Controls.Add(Me.txtGamePath)
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Location = New System.Drawing.Point(12, 11)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(391, 137)
        Me.GroupBox2.TabIndex = 2
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "设置"
        '
        'btnIdPath
        '
        Me.btnIdPath.Location = New System.Drawing.Point(295, 62)
        Me.btnIdPath.Name = "btnIdPath"
        Me.btnIdPath.Size = New System.Drawing.Size(75, 23)
        Me.btnIdPath.TabIndex = 8
        Me.btnIdPath.Text = "选择路径"
        Me.btnIdPath.UseVisualStyleBackColor = True
        '
        'btnGamePath
        '
        Me.btnGamePath.Location = New System.Drawing.Point(295, 28)
        Me.btnGamePath.Name = "btnGamePath"
        Me.btnGamePath.Size = New System.Drawing.Size(75, 23)
        Me.btnGamePath.TabIndex = 7
        Me.btnGamePath.Text = "选择路径"
        Me.btnGamePath.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.ForeColor = System.Drawing.Color.Red
        Me.Label3.Location = New System.Drawing.Point(23, 100)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(299, 12)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = "账号格式: 账号 密码 大区 小区 (空格分开,一行一个)"
        '
        'txtIdPath
        '
        Me.txtIdPath.Location = New System.Drawing.Point(80, 64)
        Me.txtIdPath.Name = "txtIdPath"
        Me.txtIdPath.Size = New System.Drawing.Size(208, 21)
        Me.txtIdPath.TabIndex = 5
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(21, 67)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(53, 12)
        Me.Label1.TabIndex = 5
        Me.Label1.Text = "账号路径"
        '
        'txtGamePath
        '
        Me.txtGamePath.Location = New System.Drawing.Point(80, 30)
        Me.txtGamePath.Name = "txtGamePath"
        Me.txtGamePath.Size = New System.Drawing.Size(208, 21)
        Me.txtGamePath.TabIndex = 4
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(21, 30)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(53, 12)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "游戏路径"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.ckbLog)
        Me.GroupBox3.Location = New System.Drawing.Point(12, 163)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(749, 48)
        Me.GroupBox3.TabIndex = 3
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "其他"
        '
        'ckbLog
        '
        Me.ckbLog.AutoSize = True
        Me.ckbLog.Location = New System.Drawing.Point(22, 21)
        Me.ckbLog.Name = "ckbLog"
        Me.ckbLog.Size = New System.Drawing.Size(72, 16)
        Me.ckbLog.TabIndex = 0
        Me.ckbLog.Text = "开启日志"
        Me.ckbLog.UseVisualStyleBackColor = True
        '
        'OpenFileDialogGamePath
        '
        Me.OpenFileDialogGamePath.Filter = "执行文件(*.exe)|*.exe"
        '
        'OpenFileDialogIdPath
        '
        Me.OpenFileDialogIdPath.Filter = "文本文件(*.txt)|*.txt"
        '
        'btnSaveini
        '
        Me.btnSaveini.Location = New System.Drawing.Point(556, 225)
        Me.btnSaveini.Name = "btnSaveini"
        Me.btnSaveini.Size = New System.Drawing.Size(75, 23)
        Me.btnSaveini.TabIndex = 4
        Me.btnSaveini.Text = "保存配置"
        Me.btnSaveini.UseVisualStyleBackColor = True
        '
        'btnTest
        '
        Me.btnTest.Location = New System.Drawing.Point(248, 225)
        Me.btnTest.Name = "btnTest"
        Me.btnTest.Size = New System.Drawing.Size(75, 23)
        Me.btnTest.TabIndex = 5
        Me.btnTest.Text = "测试"
        Me.btnTest.UseVisualStyleBackColor = True
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.dgwAccount)
        Me.GroupBox4.Location = New System.Drawing.Point(12, 254)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(749, 271)
        Me.GroupBox4.TabIndex = 7
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "游戏账号"
        '
        'dgwAccount
        '
        Me.dgwAccount.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgwAccount.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.账户名, Me.Column2, Me.Column3, Me.Column4, Me.Column5, Me.姓名, Me.身份证})
        Me.dgwAccount.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgwAccount.Location = New System.Drawing.Point(3, 17)
        Me.dgwAccount.Name = "dgwAccount"
        Me.dgwAccount.RowTemplate.Height = 23
        Me.dgwAccount.Size = New System.Drawing.Size(743, 251)
        Me.dgwAccount.TabIndex = 0
        '
        '账户名
        '
        Me.账户名.HeaderText = "游戏账户"
        Me.账户名.Name = "账户名"
        '
        'Column2
        '
        Me.Column2.HeaderText = "游戏密码"
        Me.Column2.Name = "Column2"
        '
        'Column3
        '
        Me.Column3.HeaderText = "大区"
        Me.Column3.Name = "Column3"
        '
        'Column4
        '
        Me.Column4.HeaderText = "小区"
        Me.Column4.Name = "Column4"
        '
        'Column5
        '
        Me.Column5.HeaderText = "状态"
        Me.Column5.Name = "Column5"
        '
        '姓名
        '
        Me.姓名.HeaderText = "姓名"
        Me.姓名.Name = "姓名"
        '
        '身份证
        '
        Me.身份证.HeaderText = "身份证"
        Me.身份证.Name = "身份证"
        '
        'btnLoadAccount
        '
        Me.btnLoadAccount.Location = New System.Drawing.Point(423, 225)
        Me.btnLoadAccount.Name = "btnLoadAccount"
        Me.btnLoadAccount.Size = New System.Drawing.Size(92, 23)
        Me.btnLoadAccount.TabIndex = 8
        Me.btnLoadAccount.Text = "加载游戏账号"
        Me.btnLoadAccount.UseVisualStyleBackColor = True
        '
        'mm_ly
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(768, 551)
        Me.Controls.Add(Me.btnLoadAccount)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.btnTest)
        Me.Controls.Add(Me.btnSaveini)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.btnStart)
        Me.Name = "mm_ly"
        Me.Text = "Form1"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        CType(Me.dgwAccount, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnStart As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents lblSn As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents lblNeiwang As System.Windows.Forms.Label
    Friend WithEvents txtDatiIp As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtIdPath As System.Windows.Forms.TextBox
    Friend WithEvents txtGamePath As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents ckbLog As System.Windows.Forms.CheckBox
    Friend WithEvents OpenFileDialogGamePath As System.Windows.Forms.OpenFileDialog
    Friend WithEvents OpenFileDialogIdPath As System.Windows.Forms.OpenFileDialog
    Friend WithEvents btnIdPath As System.Windows.Forms.Button
    Friend WithEvents btnGamePath As System.Windows.Forms.Button
    Friend WithEvents txtDatingSn As System.Windows.Forms.TextBox
    Friend WithEvents btnSaveini As System.Windows.Forms.Button
    Friend WithEvents btnTest As System.Windows.Forms.Button
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents dgwAccount As System.Windows.Forms.DataGridView
    Friend WithEvents 账户名 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 姓名 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 身份证 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnLoadAccount As System.Windows.Forms.Button

End Class
